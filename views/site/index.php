<?php

/* @var $this yii\web\View */
use yii\helpers\html;
$this->title = 'My Yii Application';
?>
<div class="site-index">

    <div class="jumbotron">
        <h1>Practica 4</h1>

        <p class="lead"></p>

        <p></p>
    </div>

    <div class="body-content">

        <?= Html::a('Públicos', ['site/publicos'], ['class' => 'btn btn-lg btn-success']) ?>
        <?= Html::a('Privados', ['site/privados'], ['class' => 'btn btn-lg btn-success']) ?>
        <?= Html::a('Admin', ['site/admin'], ['class' => 'btn btn-lg btn-success']) ?>
        
    </div>

    
</div>